<div align="center">

## Mathematics (2/179)

| Problem                                                           |  C  | C#  | Java | Python |
| ----------------------------------------------------------------- | :-: | :-: | :--: | :----: |
| [📂](./1069%20-%20Diamonds%20and%20Sand) 1069 - Diamonds and Sand | ✅  | ✅  |  ❌  |   ❌   |

</div>
